<?php
namespace Avanti\NewsletterGuest\Controller\Newsletter;

use Magento\Customer\Api\AccountManagementInterface as CustomerAccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Validator\EmailAddress as EmailValidator;
use Magento\Framework\View\Result\PageFactory;
use Magento\Newsletter\Model\Subscriber;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;

class Save extends Action
{
    protected $pageFactory;
    protected $emailValidator;
    protected $storeManager;
    protected $customerSession;
    protected $customerAccountManagement;
    protected $subscribeFactory;
    protected $jsonResultFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        CustomerAccountManagement $customerAccountManagement,
        SubscriberFactory $subscribeFactory,
        JsonFactory $jsonResultFactory,
        EmailValidator $emailValidator = null
    )
    {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->emailValidator = $emailValidator ?: ObjectManager::getInstance()->get(EmailValidator::class);
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->subscribeFactory = $subscribeFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            $data = ['message' => 'Invalid Request', 'status' => false];
            $result = $this->jsonResultFactory->create();
            $result->setData($data);
            return $result;
        }

        $email = (string)$this->getRequest()->getPost('email');
        $name = (string)$this->getRequest()->getPost('name');
        $status = true;
        try {
            $this->validateEmailFormat($email);
        } catch (LocalizedException $e) {
            $message = __('Please enter a valid email address.');
            $status = false;
        }

        try {
            $this->validateEmailAvailable($email);
        } catch (LocalizedException $e) {
            $message = __("This email address is already assigned to another user.");
            $status = false;
        }

        if ($status) {
            try {
                $subscriber = $this->subscribeFactory->create()->loadByEmail($email);
                if ($subscriber->getId()
                    && (int)$subscriber->getSubscriberStatus() === Subscriber::STATUS_SUBSCRIBED
                ) {
                    $data = ['message' => __("This email address is already assigned to another user."), 'status' => false];
                    $result = $this->jsonResultFactory->create();
                    $result->setData($data);
                    return $result;
                }

                $status = (int)$this->subscribeFactory->create()->subscribe($email);
                $message = $this->getSuccessMessage($status);
                $status = true;
                try {
                    $subcriber = $this->subscribeFactory->create()->loadByEmail($email);
                    $subcriber->setData('subscriber_name', $name);
                    $subcriber->save();
                } catch (\Exception $e) {
                    $message = __('Something went wrong with the subscription.');
                    $status = false;
                }
            } catch (\Exception $e) {
                $message = __('Something went wrong with the subscription.');
                $status = false;
            }
        }

        $data = ['message' => $message, 'status' => $status];
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }

    /**
     * Validates that the email address isn't being used by a different account.
     *
     * @param string $email
     * @throws LocalizedException
     * @return void
     */
    protected function validateEmailAvailable($email)
    {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        if ($this->customerSession->isLoggedIn()
            && ($this->customerSession->getCustomerDataObject()->getEmail() !== $email
                && !$this->customerAccountManagement->isEmailAvailable($email, $websiteId))
        ) {
            throw new LocalizedException(
                __('This email address is already assigned to another user.')
            );
        }
    }

    /**
     * Validates the format of the email address
     *
     * @param string $email
     * @throws LocalizedException
     * @return void
     */
    protected function validateEmailFormat($email)
    {
        if (!$this->emailValidator->isValid($email)) {
            throw new LocalizedException(__('Please enter a valid email address.'));
        }
    }

    /**
     * Get success message
     *
     * @param int $status
     * @return Phrase
     */
    private function getSuccessMessage(int $status)
    {
        if ($status === Subscriber::STATUS_NOT_ACTIVE) {
            return __('The confirmation request has been sent.');
        }
        return __('Thank you for your subscription.');
    }
}
