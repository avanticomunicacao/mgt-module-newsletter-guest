<?php
namespace Avanti\NewsletterGuest\Plugin\Newsletter\Controller;

use Magento\Newsletter\Controller\Subscriber\NewAction as NewActionCore;
use Magento\Newsletter\Model\SubscriberFactory;
use Psr\Log\LoggerInterface;

class NewAction
{
    private $subscribeFactory;
    private $logger;

    public function __construct(
        SubscriberFactory $subscriberFactory,
        LoggerInterface $logger
    ) {
        $this->subscribeFactory = $subscriberFactory;
        $this->logger = $logger;
    }

    public function afterExecute(NewActionCore $subject, $result)
    {
        if (!isset($_POST['name'])) {
            return $result;
        }
        try {
            $emaiil = $_POST['email'];
            $name = $_POST['name'];

            $subscribe = $this->subscribeFactory->create();
            $subscriber = $subscribe->loadByEmail($emaiil);

            if ($name) {
                $subscriber->setData('subscriber_name', $name);
                $subscriber->save();
            }
        } catch (\Exception $e) {
            $this->logger->error("Error in load Subscriber or add subscriber name");
        }

        return $result;
    }
}
