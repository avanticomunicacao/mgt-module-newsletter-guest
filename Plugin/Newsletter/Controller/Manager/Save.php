<?php

namespace Avanti\NewsletterGuest\Plugin\Newsletter\Controller\Manager;

use Magento\Newsletter\Controller\Manage\Save as SaveCore;
use Magento\Newsletter\Model\SubscriberFactory;
use Psr\Log\LoggerInterface;

class Save extends SaveCore
{
    private $subscribeFactory;
    private $logger;

    public function __construct(
        SubscriberFactory $subscriberFactory,
        LoggerInterface $logger
    ) {
        $this->subscribeFactory = $subscriberFactory;
        $this->logger = $logger;
    }

    public function afterExecute(SaveCore $subject, $result)
    {
        try {
            $subscribe = $this->subscribeFactory->create();

            $customerSession = $subject->_customerSession;
            $name = $customerSession->getCustomer()->getName();
            $email = $customerSession->getCustomer()->getEmail();

            $subcriber = $subscribe->loadByEmail($email);

            $subcriber->setData('subscriber_name', $name);
            $subcriber->save();
        } catch (\Exception $e) {
            $this->logger->error("Error in load Subscriber or add subscriber name");
        }

        return $result;
    }
}
