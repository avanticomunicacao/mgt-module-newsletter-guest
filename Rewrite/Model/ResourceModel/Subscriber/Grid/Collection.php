<?php
namespace Avanti\NewsletterGuest\Rewrite\Model\ResourceModel\Subscriber\Grid;

use Magento\Newsletter\Model\ResourceModel\Subscriber\Grid\Collection as CollectionCore;

class Collection extends CollectionCore
{
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->_map['fields']['subscriber_name'] = 'main_table.subscriber_name';
        return $this;
    }
}
